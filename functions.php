<?php
require_once "nav-walker.php";

add_action('after_setup_theme','theme_start', 16);

function theme_start(){
	add_template_support();
}

/*********************
	THEME SUPPORT
*********************/

// Adding WP 3+ Functions & Theme Support
function add_template_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support('post-thumbnails');

	// rss
	add_theme_support('automatic-feed-links');

	//custom logo

	add_theme_support( 'custom-logo', array(
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );

	// wp menus
	add_theme_support( 'menus' );
	register_nav_menu( "top-nav", "Top Nav Menu(top-nav)" );
	register_nav_menu( "bottom-nav", "Bottom Nav Menu(bottom-nav)" );

	//html5 support (http://themeshaper.com/2013/08/01/html5-support-in-wordpress-core/)
	add_theme_support( 'html5',
	         array(
	         	'comment-list',
	         	'comment-form',
	         	'search-form',
	         )
	);

	function add_svg_to_upload_mimes( $upload_mimes ) {
		$upload_mimes['svg'] = 'image/svg+xml';
		$upload_mimes['svgz'] = 'image/svg+xml';
		return $upload_mimes;
	}
	add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );


} /* end bridgemachine theme support */

function lg_enqueue_styles_scripts() {
    
    // Child theme style sheet setup.
    $parent_style = '_s-style';
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), '1.4' );

	// Fonts <link href="" rel="stylesheet">
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i', false );

	// Register Scripts 
	wp_register_script( 'lg-scripts', get_stylesheet_directory_uri() . "/js/lg-scripts-min.js", array('jquery'), false );
	wp_register_script( 'kelvin-js-collection', get_stylesheet_directory_uri() . "/js/kelvin-js-collection-min.js", [], false );
    wp_register_script( 'vendorJs', get_stylesheet_directory_uri() . "/js/vendor.min.js", array('jquery'), '3.3.7' );
    wp_register_script( 'tracking', get_stylesheet_directory_uri() . "src/js/event-tracking.js", array('jquery'), '3.3.7' );

	// Enqueue Scripts.
	wp_enqueue_script( 'vendorJs' );
	wp_enqueue_script( 'lg-scripts' );
	wp_enqueue_script( 'tracking' );
	wp_enqueue_script( 'kelvin-js-collection' );
}
add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


//Dequeue JavaScripts
function project_dequeue_unnecessary_scripts() {
	wp_dequeue_script( '_s-navigation' );
	wp_deregister_script( '_s-navigation' );
}
add_action( 'wp_print_scripts', 'project_dequeue_unnecessary_scripts' );

// Format Phone Numbers Function
function format_phone($phone)
{
	$phone = preg_replace("/[^0-9]/", "", $phone);
 
	if(strlen($phone) == 7)
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
	elseif(strlen($phone) == 10)
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
	else
		return $phone;
}

// Register Custom Navigation Walker
require_once(get_stylesheet_directory() . '/inc/wp-bootstrap-navwalker.php');

// This theme uses wp_nav_menu() in one location.
register_nav_menus( array(
	'footer-1' => esc_html__( 'Footer', '_s' ),
) );

function main_nav_items ( $items, $args ) {
    if ($args->menu == 'main-menu') {
        $items .= '<li class="mobile-header-phone mr-sm"><a class="cta-lg" href="tel:+1'.do_shortcode('[lg-phone-main]').'"><span  itemprop="telephone"><span class="phone-icon"><img src="'.get_stylesheet_directory_uri().'/images/phoneicon.svg" alt=""></span> '.format_phone(do_shortcode('[lg-phone-main]')).'</a></span></li><li class="mobile-header-phone"><a href="https://skinmethod.janeapp.com/" class="cta-lg" target="_blank">Book Now</a></li>';
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'main_nav_items', 10, 2 );

// remove "Private: " from titles
function remove_private_prefix($title) {
	$title = str_replace('Private: ', '', $title);
	return $title;
}
add_filter('the_title', 'remove_private_prefix');

//Taxonomy
function create_taxonomy(){
	//Blog Category
	register_taxonomy(
		'blog-category',
		'blog',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'blog-category' ),
			'hierarchical' => true,
		)
	);

	//Product Category
	register_taxonomy(
		'product-category',
		'product',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'product-category' ),
			'hierarchical' => true,
		)
	);

	//Blog Tag
	register_taxonomy(
		'blog-tag',
		'blog',
		array(
			'label' => __( 'Tag' ),
			'rewrite' => array( 'slug' => 'blog-tag' ),
			'hierarchical' => false,
		)
	);

	//Team Category
	register_taxonomy(
		'team-category',
		'ourteam',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'team-category' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomy' );


//Custom Post Types
function create_post_type() {
  register_post_type( 'ourteam',
    array(
      'labels' => array(
        'name' => __( 'Team' ),
        'singular_name' => __( 'Team' )
      ),
      'public' => true,
      'has_archive' => false,
      'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
    )
  );

    // PRODUCTS
  	register_post_type( 'product',
        array(
            'labels' => array(
                'name'          => __( 'Products' ),
                'singular_name' => __( 'Product' )
            ),
            'public'      => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-cart',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' ),
        )
    );

    // BLOG
  	register_post_type( 'blog',
        array(
            'labels' => array(
                'name'          => __( 'Blogs' ),
                'singular_name' => __( 'Blog' )
            ),
            'public'      => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-welcome-write-blog',
            'supports' => array( 'title', 'editor', 'thumbnail', 'revisions', 'author'),
        )
    );

    // BLOG
  	register_post_type( 'method',
        array(
            'labels' => array(
                'name'          => __( 'Methods' ),
                'singular_name' => __( 'Method' )
            ),
            'public'      => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-welcome-write-blog',
            'supports' => array( 'title', 'editor', 'thumbnail', 'revisions'),
        )
    );

    // CONCERN
  	register_post_type( 'concern',
        array(
            'labels' => array(
                'name'          => __( 'Concerns' ),
                'singular_name' => __( 'Concern' )
            ),
            'public'      => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-welcome-write-blog',
            'supports' => array( 'title', 'editor', 'thumbnail', 'revisions'),
        )
    );

}
add_action( 'init', 'create_post_type' );



// ACF Options page
if( function_exists('acf_add_options_sub_page') ) {

 	// add parent
	$parent = acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title' 	=> 'Options',
		'redirect' 		=> false
	));
	
	
	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Vendor Logos',
		'menu_title' 	=> 'Vendor Logos',
		'parent_slug' 	=> $parent['menu_slug'],
	));
	
}

/*dequeue wp-embed.js*/
function lg_deregister_embed(){
	wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'lg_deregister_embed' );

// dequeue stylesheets and scripts conditionally
function lg_conditional_scripts_styles() {
	wp_dequeue_style('wp-block-library');
}
add_action('wp_enqueue_scripts', 'lg_conditional_scripts_styles', 100);
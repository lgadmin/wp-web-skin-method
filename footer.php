<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

</div><!-- #content -->
	<?php  get_template_part( '/inc/cta-vendor-logos'); ?>
	<footer id="colophon" class="site-footer">
		<div>
			<div class="footer-utility">
				<?php get_template_part("/inc/address-card"); ?>
			</div>
			<div class="nav-footer"><?php get_template_part("/inc/subscribe-form"); ?></div>
			<div class="blog-feed"><?php get_template_part("/inc/feed-blog"); ?></div>
		</div>
		<div class="bg-gray-base footer-bottom clearfix">
			<div class="site-info"><?php get_template_part("/inc/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/inc/site-footer-longevity"); ?> </div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

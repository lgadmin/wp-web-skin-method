<?php
get_header(); ?>

<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) : the_post();
				?>
				<!-- Feature Slider -->
				<?php
					$slider = get_field('slider');
					$box_text = get_field('box_text');
					$cta_text = get_field('cta_text');
				?>
				<div id="home-feature-item">
					<?php echo do_shortcode($slider); ?>
					<div class="feature_box">
						<?php echo $box_text; ?>
						<div class="cta-wrapper">
							<a target="_blank" href="https://skinmethod.janeapp.com/" class="cta"><?php echo $cta_text; ?></a>
						</div>
					</div>
					<div class="scroll-down">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hero-arrow.png" alt="">
					</div>
				</div>
				<!-- end Feature Slider -->

				<div id="home-page-body">
					<!-- Discover your beautiful section --> 
					<?php $bg_discover_image = get_field('discover-background-image'); ?>
						<div class="bg-discover bg-gray-lighter">
							<div class="container">
								<div class="db-body">
									<h1 class="homeH1"><?php the_field('h1_body'); ?></h1>
									<?php the_field('body_copy'); ?>
								</div>

								<div class="db-grid">
									<?php get_template_part("inc/cta-checkers"); ?>
								</div>
							</div>
						</div>
					<!-- -->

					<!-- Products section -->
						<?php // get_template_part("inc/cta-product") ?>
					<!-- end Product section -->

					<!-- Instagram section -->
					<?php
						$instagram_logo = get_field('instagram_logo');
						$instagram_background = get_field('instagram_background_image');
	                ?>
						<div class="block instagram" style="background-image: url('<?php echo $instagram_background; ?>')">
							<h2>Follow us on <?php echo lg_file_get_contents( $instagram_logo ); ?><span>@skin.method</span></h2>
						</div>
						<div class="instagram-feed">
							<?php echo do_shortcode('[instagram-feed num="6"]'); ?>
						</div>
					<!-- end Instagram section -->

					<!-- Subscribe section -->
					<?php
						$mailchimp_title = get_field('mailchimp_section_title');
					?>
						<div class="block mailchimp-form container">
							<?php if($mailchimp_title): ?>
								<h2><?php echo $mailchimp_title; ?></h2>
							<?php endif; ?>
							<?php get_template_part("/inc/subscribe-form"); ?>	
						</div>
					<!-- end Subscribe section -->

					<!-- Logo carousel section -->
						<!-- todo -->
					<!-- end Logo carousel section -->
				</div>

				<?php
			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<a class="hidden" id="master-slider-event-trigger"></a>

<?php get_footer();

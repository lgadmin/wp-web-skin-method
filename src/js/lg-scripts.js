(function($) {

  $(window).load(function(){ 
        //Blog Grid
        $('.grid').masonry({
          // options... 
          itemSelector: '.grid-item',
        });
  });

  $(window).scroll(function(){
    KCollection.RepositionFeatureItem(['#masthead'],'#home-feature-item');
  });

  $(document).ready(function(){

    /* disable bootstrap click behavior, start it over */
    $(document).unbind('click.bs.dropdown.data-api');
    $('.dropdown-toggle').on('click', function(){
        var parent = $(this).closest('li.dropdown');
        parent.toggleClass('open');
        if(parent.hasClass('open')){
            parent.siblings().removeClass('open');
        }
        toggleMainNavInMobile(parent);
    });
    $('.nav-back').on('click', function(){
        var parent = $(this).closest('li.dropdown');
        parent.removeClass('open');
        toggleMainNavInMobile(parent);
    });

    function toggleMainNavInMobile(navItem){
        if(navItem.hasClass('open')){
            navItem.closest('.navbar-nav').addClass('shadow');
        }else{
            navItem.closest('.navbar-nav').removeClass('shadow');
        }
    }

    //master slider callback
    $('#master-slider-event-trigger').on('click', function(){
        KCollection.RepositionFeatureItem(['#masthead'],'#home-feature-item');

        setTimeout(function(){ 
            $('.feature_box').addClass('visible');
            $('.scroll-down').addClass('visible'); 
        },1000);
    });
  
    //home page scroll arrow click
    $('.scroll-down img').on('click', function(){
        var scrollTop = $('#home-page-body').offset().top;
        var headerHeight = $('#masthead').outerHeight();
        $("html, body").animate({ scrollTop: scrollTop - headerHeight }, 600);
    });

 });
}(jQuery));


jQuery(document).ready(function($){  
        
    jQuery('.db-body h1.homeH1').html(function(){  
        // separate the text by spaces
        var text= jQuery(this).text().split(' ');
        // drop the last word and store it in a variable
        var last = text.pop();
        // join the text back and if it has more than 1 word add the span tag
        // to the last word
        return text.join(" ") + (text.length > 0 ? ' <span class="last">'+last+'</span>' : last);   
    });
});
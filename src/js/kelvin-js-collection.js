var KCollection = (function(){
	var mobileBreakPoint = 480;
	var tabletBreakPoint = 768;
	var desktopBreakPoint = 1280;

	/**
	 * Reduce element height to 100vh - fixedItems' height. In order to cover exact one screen. 
	 * @param {Array} - Array of html elements' css query selector for fix positioned items on the screen
	 * @param {Dom Element} Target elements' css query selector for repositioning
	 */
	function RepositionFeatureItem(fixItemSelector, targetItemSelector) {
	    var windowWidth = window.innerWidth;
	    var windowHeight = window.innerHeight;
	    var targetElements = document.querySelectorAll(targetItemSelector);

	    var fixItemHeightArray = fixItemSelector.map(function(querySelector){
	    	var items = document.querySelectorAll(querySelector);

	    	if(items.length > 0){
		    	var itemsHeightArray = Array.prototype.map.call(items,function(item){
		    		return item.offsetHeight;
		    	});
		    	var itemsHeight = Array.prototype.reduce.call(itemsHeightArray, function(total, single){
		    		return total + single;
		    	});
		    	return itemsHeight;
	    	}else{
	    		return 0;
	    	}
	    });

	    var fixItemHeight = fixItemHeightArray.reduce(function(total, single){
	    	return total + single;
	    });

	    // Append Parent to target element
	    Array.prototype.every.call(targetElements, function(item){
		    var parent = item.parentNode;
		    var wrapper;

		    if(parent.className == 'k-feature-item'){
		    	wrapper = parent;
		    }else{
				wrapper = document.createElement('div');

				wrapper.setAttribute('class', 'k-feature-item');
				// set the wrapper as child (instead of the element)
				parent.replaceChild(wrapper, item);
				// set element as child of wrapper
				wrapper.appendChild(item);
		    }

	        var marginTop = (windowHeight - item.offsetHeight) / 2;

	        if (marginTop > 0) {
	            marginTop = 0;
	        }

	        if (item.offsetHeight + fixItemHeight < windowHeight) {
	        	wrapper.style.overflow = 'auto';
	        	wrapper.style.height = 'auto';
	        	item.style.marginTop = '0px';
	        } else if (windowWidth > tabletBreakPoint) {
	        	wrapper.style.overflow = 'hidden';
	        	wrapper.style.height = (windowHeight - fixItemHeight) + 'px';
	        	item.style.marginTop = marginTop + 'px';
	        }
	    });
	}

  return {
    RepositionFeatureItem: RepositionFeatureItem
  };
}());
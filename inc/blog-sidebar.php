<!-- Side Bar -->
	<div class="widget-area blog-sidebar">

		<!-- Contact Form -->
		<section class="contact">
			<div class="form-wrapper" style="padding:0;">
				<h2>Contact Us</h2>
				<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
			</div>
		</section>
		<!-- end Contact Form -->

		<!-- Recent Post -->
		<section class="recent_post">
			<h2>RECENT POSTS</h2>
			<?php
				while ( have_posts() ) : the_post();
					$news = array();
					$args = array(
			            'showposts'	=> -1,
			            'post_type'		=> 'blog',
			        );
			        $result = new WP_Query( $args );

			        // Loop
			        if ( $result->have_posts() ) :
			        	?>
				        	<ul>
					        	<?php
						            while( $result->have_posts() ) : $result->the_post();
							            $title = get_the_title();
							            $url = get_permalink();
							        	?>
							        		<li><a href="<?php echo $url; ?>"><?php echo $title; ?></a></li>
										<?php
						            endwhile;
					            ?>
				            </ul>
			            <?php
			        endif; // End Loop

			        wp_reset_query();
				endwhile; // End of the loop.
				?>
		</section>
		<!-- end Recent Post -->

		<!-- Categories -->
		<section class="categories">
			<h2>CATEGORIES</h2>
			<ul>
			<?php
				$terms = get_terms(array(
                  'taxonomy' => 'blog-category',
                  'hide_empty' => false,)
				);
				 foreach($terms as $term){
				    ?>
						<li><a href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->name; ?></a></li>
				    <?php
				  }
			?>
			</ul>
		</section>
		<!-- end Categories -->

		<!-- Tags -->
		<section class="tags">
			<h2>TAGS</h2>
			<ul>
			<?php
				$terms = get_terms(array(
                  'taxonomy' => 'blog-tag',
                  'hide_empty' => false,)
				);
				 foreach($terms as $term){
				    ?>
						<li><a href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->name; ?></a></li>
				    <?php
				  }
			?>
			</ul>
		</section>
		<!-- end Tags -->

	</div>
<!-- end Side Bar -->
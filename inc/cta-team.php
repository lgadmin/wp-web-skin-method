<section class="ourteam-cta pb-lg">

	<h2 class="text-center">Meet our doctors</h2>

	<?php $loop = new WP_Query(
		array(
			'post_type' => 'ourteam',
			'posts_per_page' => 10,
			'tax_query' => array(
						        array (
						            'taxonomy' => 'team-category',
						            'field' => 'slug',
						            'terms' => 'doctor',
						            'operator' => 'IN'
						        )
    		)
    ) ); ?>

	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
	<?php
		$short_description = get_field('short_description');
	?>

<div class="type-ourdoctor pt-md text-center">

	<div class="split-content">
		<div class="half-image">
			<div class="fimage-cont">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> <?php the_post_thumbnail(); ?> </a>
			</div>
		</div>
		<div class="half-copy">
			<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
			<?php the_excerpt(); ?>
			<div class="short-description">
				<?php echo $short_description; ?>
			</div>
		</div>
	</div>

</div>

	<?php endwhile; ?>
	<?php wp_reset_query(); ?>

</section>

<section class="ourteam-cta">

	<h2 class="text-center">Meet our team</h2>

	<?php $loop = new WP_Query(
		array(
			'post_type' => 'ourteam',
			'posts_per_page' => 10,
			'tax_query' => array(
						        array (
						            'taxonomy' => 'team-category',
						            'field' => 'slug',
						            'terms' => 'doctor',
						            'operator' => 'NOT IN'
						        )
    		)
    ) ); ?>

	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

<div class="type-ourteam text-center">

    <header class="entry-header">
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="fimage-cont">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> <?php the_post_thumbnail(); ?> </a>
			</div>
		<?php endif; ?>
    </header>

	<div class="entry-content">
		<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
		<?php the_excerpt(); ?>
	</div>

</div>

	<?php endwhile; ?>
	<?php wp_reset_query(); ?>

</section>

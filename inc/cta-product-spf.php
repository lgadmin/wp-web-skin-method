<div class="products-cta block container">
	
	<h2>Products we love</h2>
	<p>Featuring our current product obsessions!</p>

	<?php $loop = new WP_Query( array( 'post_type' => 'product', 'posts_per_page' => -1, 'tax_query'=> array(
        array(
	        'taxonomy' => 'product-category',
	        'terms' => array('dna-repair'),
	        'field' => 'slug',
	    )
    )) ); ?>

	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

	<div class="type-products text-center">
		    <header class="entry-header">
		        <?php 
					echo '<div class="fimage-cont">';
					the_post_thumbnail();
					echo '</div>';
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); 
				?>
		    </header>

			<div class="entry-content">
				<a href="<?php echo get_permalink(); ?>" class="cta">More info</a>
			</div>
	</div>

	<?php endwhile; ?>
	<?php wp_reset_query(); ?>

</div>
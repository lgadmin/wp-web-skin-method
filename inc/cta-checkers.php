<div class="cta-checkers">
	<div class="checkers-cont">
	    <?php while( have_rows('checker') ): the_row(); ?>
			<a href="<?php the_sub_field('checker_link'); ?>" class="checker">
				<?php if ( get_sub_field('checker_image') ): ?>
					<div class="overlay">
						<div class="img-cont">
							<?php echo wp_get_attachment_image(get_sub_field('checker_image'), 'full'); ?>
						</div>
					</div>
				<?php endif ?>
				<span><?php the_sub_field('checker_title'); ?></span>
			</a>      
	    <?php endwhile; ?>
	</div>
</div>
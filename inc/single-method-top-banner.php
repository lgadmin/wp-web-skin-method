<?php
	$title = get_the_title();
	$feature_image =  get_the_post_thumbnail_url();
?>

<div class="internal-top-banner">
	<div class="feature-image" style="background-image: url('<?php echo $feature_image; ?>');">
		
	</div>
	<div class="title-section">
		<h2>Our Methods</h2>
		<p><?php echo $title; ?></p>
	</div>
</div>
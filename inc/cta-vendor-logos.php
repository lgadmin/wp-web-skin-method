

<?php if(have_rows('vendors', 'option')): ?>

	<script>
		jQuery(document).ready(function(){
		  jQuery('.vendor-logos').slick({
		      slidesToShow: 6,
		      autoplay: true,
		      responsive: [
		      	{
			      breakpoint: 1000,
			      settings: {
					slidesToShow: 4,
			      }
			    },
			    {
			      breakpoint: 768,
			      settings: {
					slidesToShow: 3,
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
					slidesToShow: 2,
			      }
			    },
		      ]
		  });
		});
	</script>

	<div class="bg-gray-lighter pt-md pb-md pl-lg pr-lg">
		<div class="vendor-cont">
			<section class="vendor-logos">
			    <?php while( have_rows('vendors', 'option') ): the_row(); ?>
					<div><a target="_blank" href="<?php the_sub_field('vendor_link'); ?>"><?php echo wp_get_attachment_image(get_sub_field('vendor_image'), 'full'); ?></a></div>        
			    <?php endwhile; ?>
			</section>
		</div>
	</div>

<?php endif; ?>

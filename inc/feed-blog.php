<div class="feed">
	<h2>Follow us on <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/instagram-reverse.svg"><span>@skin.method</span></h2>
	<?php echo do_shortcode('[instagram-feed num="4"]'); ?>
</div>
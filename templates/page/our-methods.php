<?php
get_header(); ?>
<div id="content" class="site-content methods">
	<?php
		get_template_part( '/inc/internal-page-top-banner' );
	?>
	<div id="primary">
		<main id="main" class="site-main">
			<?php
				if( have_rows('page_layout') ):
				    while ( have_rows('page_layout') ) : the_row();
						$background_color = get_sub_field('background_color');
						$full_width = get_sub_field('full_width');
						$no_padding_top = get_sub_field('no_padding_top');
						$no_padding_bottom = get_sub_field('no_padding_bottom');
						$order_reverse = get_sub_field('order_reverse');
				        $titie = get_sub_field('title');
				        $content = get_sub_field('content');
				        $methods = get_sub_field('methods');
				        $image = get_sub_field('image');
				        $image_link = get_sub_field('image_link');
				        $cta_enable = get_sub_field('cta_enable');
				        $cta_text = get_sub_field('cta_text');
				        $cta_link = get_sub_field('cta_link');
				        ?>
				        <div class="<?php echo $background_color; ?>">
							<div class="split-content block <?php if(!$full_width){echo 'container';} ?> <?php if($no_padding_top){echo 'no-pt';} ?> <?php if($no_padding_bottom){echo 'no-pb';} ?> <?php if($order_reverse){echo 'reverse';} ?>">
								<div class="half-image">
									<?php if($image_link): ?>
										<a href="<?php echo $image_link; ?>"><img src="<?php echo $image; ?>" alt=""></a>
									<?php else: ?>
										<img src="<?php echo $image; ?>" alt="">
									<?php endif; ?>
								</div>
								<div class="half-copy">
									<?php if($titie): ?>
										<h2><?php echo $titie; ?></h2>
									<?php endif; ?>
									<?php if($content): ?>
										<?php echo $content; ?>
									<?php endif; ?>

									<?php
									if($methods){
										/*while ( have_posts() ) : the_post();
											$news = array();
											$args = array(
									            'numberposts'	=> -1,
									            'post_type'		=> 'method',
									            'category_name' => $methods->slug,
									            'post_status' => array('publish', 'private'),
									        );
									        $result = new WP_Query( $args );

									        // Loop
									        if ( $result->have_posts() ) :
									            while( $result->have_posts() ) : $result->the_post();
									            $short_description = get_field('short_description');
									        	?>
									        		<h3><?php echo get_the_title(); ?></h3>
									        		<p><?php echo $short_description; ?><?php if($post->post_status == 'publish'): ?>  |  <a href="<?php echo get_permalink(); ?>">LEARN MORE</a></p><?php endif; ?>
												<?php
									            endwhile;
									        endif; // End Loop

									        wp_reset_query();
										endwhile; // End of the loop.*/
										foreach ($methods as $method) {
											$short_description = get_field('short_description', $method);
										    ?>
												<h3><?php echo get_the_title($method); ?></h3>
									        	<p><?php echo $short_description; ?><?php if(get_post_status($method) == 'publish'): ?>  |  <a href="<?php echo get_permalink($method); ?>">LEARN MORE</a></p><?php endif; ?>
										    <?php
										}
									}
									?>

									<?php if($cta_enable): ?>
										<p>
											<a class="cta" href="<?php echo $cta_link; ?>"><?php echo $cta_text; ?></a>
										</p>
									<?php endif; ?>
								</div>
							</div>
						</div>
				        <?php
				    endwhile;
				else :
				    // no rows found
				endif;
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<?php get_footer();

<?php
get_header(); ?>

<div id="content" class="site-content">
	<div id="primary">
		<main id="main" class="site-main about-us">
			<?php
			while ( have_posts() ) : the_post();
				$feature_banner = get_field('feature_banner');
				$contact_form = get_field('contact_form');
				$map = get_field('map');
				$logo = get_field('logo');
				?>
					<!-- Banner -->
					<div class="featured-image" style="background-image:url('<?php echo $feature_banner; ?>');">
						<div class="form-wrapper">
							<h2>Contact Us</h2>
							<?php echo do_shortcode($contact_form); ?>
						</div>
					</div>
					<!-- end Banner -->

					<!-- Contact -->
						<div class="block container contact-section">
							<div class="map">
								<?php if($map): ?>
									<?php echo do_shortcode($map); ?>
								<?php endif; ?>
							</div>
							<div class="contact">
								<?php 
									$address1 = do_shortcode('[lg-address1]');
									$address2 = do_shortcode('[lg-address2]');
									$city = do_shortcode('[lg-city]');
									$province = do_shortcode('[lg-province]');
									$postcode = do_shortcode('[lg-postcode]');
									$phone = do_shortcode('[lg-phone-main]');
									$email = do_shortcode('[lg-email]');
								?>
								<img src="<?php echo $logo; ?>" alt="Skin Method Logo">
								<address>
									<?php echo ($address1.', '.$address2.', '.$city.', '.$province.', '.$postcode); ?>
									<br>
									<?php
										echo ('<a href="tel:'.$phone.'">'.$phone.'</a> | <a href="mailto:'.$email.'">'.$email.'</a>');
									?>
								</address>

								<div class="hours">
									<h2><i class="fa fa-clock-o" aria-hidden="true"></i> OUR HOURS</h2>
									<?php
									if( have_rows('hours') ):
										?>
										<ul style="overflow: auto;">
										<?php
									    while ( have_rows('hours') ) : the_row();
									        $day = get_sub_field('day');
									        $hours = get_sub_field('hours');
									        ?>
												<li><?php echo $day; ?>  <span class="bold"><?php echo $hours; ?></span></li>
									        <?php
									    endwhile;
									    ?>
 										</ul>
									    <?php
									else :
									    // no rows found
									endif;

									?>
								</div>

								<!--<div class="pt-lg small"><strong>HOLIDAY OFFICE CLOSURE - We will be closed DEC 23, 2018 - JAN 03, 2019.</strong></div>-->
							</div>
						</div>
					<!-- end Contact -->

				<?php
			endwhile;
			?>
		</main>
	</div>
</div>

<?php get_footer();

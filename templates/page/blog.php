<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */
get_header(); ?>
<div id="content" class="site-content blog">
	<?php
		get_template_part( '/inc/internal-page-top-banner' );
	?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main container block">

			<?php
				$args = array(
		            'showposts'	=> -1,
		            'post_type'		=> 'blog',
		        );
		        $result = new WP_Query( $args );

		        // Loop
		        if ( $result->have_posts() ) :
		        	?>
		        	<div class="grid">
		        	<?php
		            while( $result->have_posts() ) : $result->the_post();
		            $short_description = get_field('blog_short_description');
		        	?>
		        		<div class="grid-item">
		        			<div>
		        				<a href="<?php echo get_permalink(); ?>">
		        					<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
		        				</a>
		        				<div class="details">
			        				<h2><?php echo get_the_title(); ?></h2>
			        				<p><?php echo $short_description; ?></p>
			        			</div>
			        			<div class="date">
		        					<?php echo get_the_date(); ?>
		        				</div>
		        			</div>
		        		</div>
					<?php
		            endwhile;
		            ?>
		            </div>
		            <?php
		        endif; // End Loop

		        wp_reset_query();
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<?php get_footer();

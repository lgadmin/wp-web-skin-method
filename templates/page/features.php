<?php
get_header(); ?>
<div id="content" class="site-content features">
	<?php
		get_template_part( '/inc/internal-page-top-banner' );
	?>
	<div id="primary">
		<main id="main" class="site-main">
			<?php
				$intro_title = get_field('intro_title');
				$intro_content = get_field('intro_content');
			?>
			<div class="block container">
				<?php if($intro_title): ?>
					<h2><?php echo $intro_title; ?></h2>
				<?php endif; ?>
				<?php if($intro_content): ?>
					<?php echo $intro_content; ?>
				<?php endif; ?>
			</div>

			<?php
				if( have_rows('sections') ):
					$i = 0;
				    while ( have_rows('sections') ) : the_row();
				        $image = get_sub_field('image');
				        $title = get_sub_field('title');
				        $content = get_sub_field('content');
				        ?>
							<div class="block split-content container no-pt <?php if($i % 2 == 1){echo 'reverse';} ?>">
								<div class="half-image">
									<img src="<?php echo $image; ?>" alt="">
								</div>
								<div class="half-copy">
									<?php if($title): ?>
										<h2><?php echo $title; ?></h2>
									<?php endif; ?>
									<?php if($content): ?>
										<?php echo $content; ?>
									<?php endif; ?>
									<div class="charge">
										<?php
											if( have_rows('charge') ): 
											    while ( have_rows('charge') ) : the_row();
											        $short_description = get_sub_field('short_description');
											        $dollar_amount = get_sub_field('dollar_amount');
											        $long_description = get_sub_field('long_description');
											        ?>
											        <div class="single-charge">
											        	<div class="description-left">
															<div class="short-description">
																<?php echo $short_description; ?>
															</div>
															<div class="dollar-amount">
																<?php echo $dollar_amount; ?>
															</div>
														</div>
														<div class="description-right">
															<?php echo $long_description; ?>
														</div>
											        </div>
											        <?php
											    endwhile;
											else :
											    // no rows found
											endif;
										?>
									</div>
								</div>
							</div>
				        <?php
				        $i++;
				    endwhile;
				else :
				    // no rows found
				endif;
			?>

			<!-- Products section -->
				<?php get_template_part("inc/cta-product") ?>
			<!-- end Product section -->

		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<?php get_footer();

<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<?php
		get_template_part( '/inc/internal-page-top-banner' );
	?>
	<div id="primary" class="content-area page-about">
		<main id="main" class="site-main">
		
			<div class="container block">
					<?php
					while ( have_posts() ) : the_post();

						the_content();

					endwhile; // End of the loop.
					?>
			</div>

			<div class="container">
				<?php echo do_shortcode('[masterslider id="2"]'); ?>
			</div>
			

			<div class="container block">
				<?php get_template_part("inc/cta-team"); ?>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<?php get_footer();

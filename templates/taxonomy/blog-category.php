<?php
get_header(); ?>
<div class="page-template-tpl-container-sidebar container left-block blog">
	<div id="content" class="site-content">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<div class="blog-filter">
				<?php
					$tax = get_queried_object();
				?>
					<h2>Blog Category: <?php echo $tax->name; ?></h2>
				</div>
				<?php
					$args = array(
			            'numberposts'	=> -1,
			            'post_type'		=> 'blog',
			            'tax_query' => array(
								array(
									'taxonomy' => $tax->taxonomy,
									'field'    => 'slug',
									'terms'    => $tax->slug,
								),
							),
			        );
			        $result = new WP_Query( $args );

			        // Loop
			        if ( $result->have_posts() ) :
			        	?>
			        	<div class="grid">
			        	<?php
			            while( $result->have_posts() ) : $result->the_post();
			            $short_description = get_field('blog_short_description');
			        	?>
			        		<div class="grid-item">
			        			<div>
			        				<a href="<?php echo get_permalink(); ?>">
			        					<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
			        				</a>
			        				<div class="details">
				        				<h2><?php echo get_the_title(); ?></h2>
				        				<p><?php echo $short_description; ?></p>
				        			</div>
				        			<div class="date">
			        					<?php echo get_the_date(); ?>
			        				</div>
			        			</div>
			        		</div>
						<?php
			            endwhile;
			            ?>
			            </div>
			            <?php
			        endif; // End Loop

			        wp_reset_query();
				?>

			</main>
		</div>
		<?php get_template_part( '/inc/blog-sidebar' ); ?>
	</div>
	<?php 
		get_footer();
	?>
</div>

<?php

get_header(); ?>
	<div id="primary" class="site-content concern">
		<?php
			get_template_part( '/inc/single-concern-top-banner' );
		?>
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
		?>

		<!-- Main -->
			<?php
				$description = get_field('description');
			?>
			<div class="block container">
				<h1><?php echo get_the_title(); ?></h1>
				<?php echo $description; ?>

				<a target="_blank" href="https://skinmethod.janeapp.com/" class="cta mt-md">Book your Consultation</a>
			</div>
		<!-- endMain -->


		<!-- Treatment -->
		<div class="split-content container">
			<div class="half-image">
				<img src="<?php echo get_field('treatment_image'); ?>" alt="">
			</div>
			<div class="half-copy">
				<div class="treatment">
					<?php 
					$posts = get_field('treatment_options');

					if( $posts ): ?>
						<h3 class="pb-md">Treatment Options:</h3>
					    <ul class="checkmark">
					    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
					        <?php setup_postdata($post); ?>
					        <li>
					            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					        </li>
					    <?php endforeach; ?>
					    </ul>
					    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<!-- end Treatment -->


		<!-- Gray Section 1 -->
			<?php
				$gray_section_1_enable = get_field('gray_section_1_enable');
				if($gray_section_1_enable){
					$gray_section_1 = get_field('gray_section_1');
					if($gray_section_1){
						$image = $gray_section_1['image'];
						$content = $gray_section_1['content'];
						?>
							<div class="split-content bg-gray-lighter">
								<div class="half-image">
									<img src="<?php echo $image; ?>" alt="">
								</div>
								<div class="half-copy">
									<?php echo $content; ?>
								</div>
							</div>
						<?php
					}
				}
			?>
		<!-- end Gray Section 1 -->

		<?php
		endwhile;
		?>

		</main>
	</div>
<?php get_footer(); ?>


<?php

get_header(); ?>
	<div id="primary" class="site-content">
		<?php
			get_template_part( '/inc/single-method-top-banner' );
		?>
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
		?>

		<!-- Main -->
			<?php
				$description = get_field('description');
			?>
			<div class="block container">
				<h1><?php echo get_the_title(); ?></h1>
				<?php echo $description; ?>

				<a target="_blank" href="https://skinmethod.janeapp.com/" class="cta mt-md">Book your Consultation</a>
			</div>
		<!-- endMain -->


		<!-- Gray Section 1 -->
			<?php
				$gray_section_1_enable = get_field('gray_section_1_enable');
				if($gray_section_1_enable){
					$gray_section_1 = get_field('gray_section_1');
					if($gray_section_1){
						$image = $gray_section_1['image'];
						$content = $gray_section_1['content'];
						?>
						<div class="bg-gray-lighter">
							<div class="split-content container">
								<div class="half-image">
									<img src="<?php echo $image; ?>" alt="">
								</div>
								<div class="half-copy">
									<?php echo $content; ?>
								</div>
							</div>
						</div>
						<?php
					}
				}
			?>
		<!-- end Gray Section 1 -->


		<!-- Youtube Video -->
		<?php
			$video = get_field('video');
		?>
		<?php if($video): ?>
		<div class="video block container">
			<?php echo $video; ?>
		</div>
		<?php endif; ?>
		<!-- end Youtube Video -->

	
		<!-- Gray Section 2 -->
		<?php
			$gray_section_2_enable = get_field('gray_section_2_enable');
			if($gray_section_2_enable){
				$gray_section_2 = get_field('gray_section_2');
				if($gray_section_2){
					$media = $gray_section_2['media'];
					$image = $gray_section_2['image'];
					$before_and_after = $gray_section_2['before_and_after'];
					$content = $gray_section_2['content'];
					?>
					<div class="bg-gray-lighter">
						<div class="split-content container	">
							<div class="half-image">
							<?php if($media == 'Image'): ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
							<?php else: ?>
								<?php if($before_and_after): ?>
									<?php echo do_shortcode($before_and_after); ?>
								<?php endif; ?>
							<?php endif; ?>
							</div>
							<div class="half-copy">
								<?php echo $content; ?>
							</div>
						</div>
					</div>
					<?php
				}
			}
		?>
		<!-- end Gray Section 2 -->

		<!-- What to expect section -->
		<?php
			$what_to_expect_section_enable = get_field('what_to_expect_section_enable');
			if($what_to_expect_section_enable){
				$what_to_expect_section = get_field('what_to_expect_section');
				if($what_to_expect_section){
					$content = $what_to_expect_section['content'];
					$boxes = $what_to_expect_section['boxes'];
					?>
					<div class="block container">
						<?php echo $content; ?>
							<?php
								if($boxes){
									?>
									<div class="boxes">
									<?php
									foreach($boxes as $row){
										echo "<div><h3>".$row['label']."</h3><p>".$row['content']."</p></div>";
									}
									?>
									</div>
									<?php
								}
							?>
						
						<a target="_blank" href="https://skinmethod.janeapp.com/" class="cta">Book your Consultation</a>
					</div>
					<?php
				}
			}
		?>
		<!-- end What to expect section -->

		<?php
		endwhile;
		?>

		</main>
	</div>
<?php get_footer(); ?>


<?php
get_header(); ?>
<div class="page-template-tpl-container-sidebar container left-block single-blog">
	<div id="content" class="site-content">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<?php
					while ( have_posts() ) : the_post();
					$thumbnail = get_the_post_thumbnail();
					$long_description = get_field('blog_long_description');
					$title = get_the_title();
					$date = get_the_date();
					?>
						<?php if($thumbnail): ?>
							<!--<div class="thumbnail">
								<?php echo $thumbnail; ?>
							</div>-->
						<?php endif; ?>
						<h2 class="blog-title">
							<span class="publish-date"><?php echo $date; ?></span><span class="title"><?php echo $title; ?></span>
							<br>
							<span class="author">
								by <?php echo get_author_name(); ?>
							</span>
						</h2>
						<?php echo $long_description; ?>
						<div class="blog-nav">
							<?php if(get_next_post()): ?>
								<a href="<?php echo get_next_post()->guid; ?>" class="previous cta">Previous Post</a>
							<?php endif; ?>
							<?php if(get_previous_post()): ?>
								<a href="<?php echo get_previous_post()->guid; ?>" class="next cta">Next Post</a>
							<?php endif; ?>
						</div>
					<?php
					endwhile;
				?>
			</main>
		</div>
		<?php get_template_part( '/inc/blog-sidebar' ); ?>
	</div>
	<?php 
		get_footer();
	?>
</div>

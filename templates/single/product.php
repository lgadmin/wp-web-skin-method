<?php

get_header(); ?>
	<div id="primary" class="site-content single-product">
		<?php
			$thumbanil = get_the_post_thumbnail();
		?>
		<main id="main" class="site-main">

			<div class="pt-lg pb-lg container" style="text-align: center;">
				<div class="split-content">
					<div class="half-image">
						<?php echo $thumbanil; ?>
					</div>
					<div class="half-copy">
						<h2 class="h4"><?php echo get_the_title(); ?></h2>
						<?php the_content(); ?>
					</div>
				</div>
			</div>

		</main>
	</div>
<?php get_footer(); ?>


// ----- gulp required var
var gulp                   = require('gulp'),
    browserSync            = require('browser-sync').create(),
    svgstore               = require('gulp-svgstore'),
    svgmin                 = require('gulp-svgmin'),
    concat                 = require('gulp-concat'),
    path                   = require('path'),
    sass                   = require('gulp-sass'),
    rename                 = require('gulp-rename'),
    uglify                 = require('gulp-uglify'),
    autoprefixer           = require('gulp-autoprefixer'),
    sourcemaps             = require('gulp-sourcemaps'),
    imagemin               = require("gulp-imagemin"),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    imageminPngquant       = require('imagemin-pngquant'),
    minify                 = require('gulp-minify'),
    notify                 = require("gulp-notify"),
    plumber                = require('gulp-plumber');

// ----- Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        open: "external",
        proxy: "skinmethod.test"
      });
});

// ----- Path Configurations
var config = {
    sassPath    : 'src/sass',
    jsPath      : 'src/js',
    jsPublic    : 'js/',
    svgPath     : '/src/svg-icons',
    nodePath    : './node_modules',
    imagePublic : 'images/',
    imageSRC    : 'src/images'
};

// ---- SASS Load Paths
var sassLoad = [
    config.nodePath + '/bootstrap-sass/assets/stylesheets/bootstrap',
    config.nodePath + '/susy/sass',
    config.nodePath + '/breakpoint-sass/stylesheets/',
    config.nodePath + '/font-awesome/scss/',
];

// ----- Get Assests
gulp.task('getassets', function() {
    gulp.src(config.nodePath + '/font-awesome/fonts/**/*.*')
    .pipe(gulp.dest('./fonts'));

    gulp.src(config.nodePath + '/slick-carousel/slick/fonts/**.*')
    .pipe(gulp.dest('./fonts'));

    gulp.src(config.nodePath + '/slick-carousel/slick/ajax-loader.gif')
    .pipe(gulp.dest(config.imageSRC));
});

// ----- SASS
gulp.task('sass', function() {
 return gulp.src(config.sassPath + '/**/*.scss')
  .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
  .pipe(sourcemaps.init())
  .pipe(sass({
    outputStyle  : 'nested',
    includePaths : sassLoad,
    onError: function(err) { return notify().write(err); }
  }))
  .pipe(autoprefixer({ browsers: ['last 3 versions']}))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('.'))
  .pipe(browserSync.stream());
});

// ----- SVG Store
gulp.task('svgstore', function () {
  return gulp
      .src(config.svgPath + '/**/*.svg')
      .pipe(svgmin(function (file) {
          var prefix = path.basename(file.relative, path.extname(file.relative));
          return {
              plugins: [{
                  cleanupIDs: {
                      prefix: prefix + '-',
                      minify: true
                  }
              }]
          }
      }))
      .pipe(svgstore())
      .pipe(gulp.dest(config.imagePublic));
});

// ----- Image optimize for google
gulp.task("imagemin", function(){
    return gulp.src( config.imageSRC + "/**/*")
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imageminJpegRecompress({
                progressive: true,
                max: 80,
                min: 70
            }),
            imageminPngquant({quality: '75-85'}),
            imagemin.svgo({plugins: [{removeViewBox: false}]})
        ]))
        .pipe(gulp.dest(config.imagePublic));
});

gulp.task('vendorJS', function() {
    return gulp.src([
            config.nodePath + '/bootstrap-sass/assets/javascripts/bootstrap.min.js',
            config.nodePath + '/masonry-layout/dist/masonry.pkgd.min.js',
            config.nodePath + '/slick-carousel/slick/slick.min.js',
        ])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(config.jsPublic))
        .pipe(rename('vendor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.jsPublic));
});

// ----- Javascript minify
gulp.task('jsminify', function() {
  gulp.src(config.jsPath + '/**/*.js')
    .pipe(minify({ noSource: true }))
    .pipe(gulp.dest(config.jsPublic))
});

// ----- Gulp Watch
gulp.task('watch', function(){
  gulp.watch(config.sassPath + '/**/*.scss', {cwd:'./'}, ['sass']);
  gulp.watch(config.imageSRC + '/**/*', {cwd:'./'}, ['imagemin']);
  gulp.watch(config.jsPath + '/**/*', {cwd:'./'}, ['jsminify']);
}); 

// ----- Default
gulp.task('default', ['browser-sync', 'sass', 'jsminify', 'imagemin', 'watch']);
// ----- Build
gulp.task('build', ['getassets', 'vendorJS', 'jsminify', 'imagemin', 'sass']);
